﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using task.Models;

namespace task.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TaskController : ControllerBase
    {
  

        [HttpPost]
        public IActionResult PostTask([FromBody] Userinfo user)
        {
            APIresult apiResult = new APIresult();

            apiResult = Utils.Validate.CheckUserInfo(user);

            ObjectResult result = new ObjectResult(apiResult);
            if (apiResult.hasErr == false)
            {
                result.ContentTypes.Add("application/json");
                if (apiResult.email == "")
                {                       
                    result.StatusCode = StatusCodes.Status401Unauthorized;
                }
                else
                {
                    Utils.RabbitMQ.publish(apiResult);
                    result.StatusCode = StatusCodes.Status200OK;
                }
            }
            else if (apiResult.hasErr == true)
            {
                result.ContentTypes.Add("application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
            }

            return result;
        }


    }
}
