﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using task.Models;

namespace task.Utils
{
    public class Validate
    {
        public static APIresult CheckUserInfo(Userinfo user)
        {
            using (var client = new HttpClient())
            {
                // Setting Base address.  
                Uri uri = new Uri("https://reqres.in/api/login");

                // Setting content type.  
                client.DefaultRequestHeaders.Accept.Clear();

                APIresult apiResult = new APIresult();

                try
                {
                    var userinfo = JsonSerializer.Serialize(user);
                    var requestContent = new StringContent(userinfo, Encoding.UTF8, "application/json");
                    var response = client.PostAsync(uri, requestContent).Result;
                   
                    //HttpResponseMessage response = client.PostAsync($"api/login").Result;
                    response.EnsureSuccessStatusCode();

                    // Verification  
                    if (response.IsSuccessStatusCode)
                    {
                        // Reading Response.  
                        string result = response.Content.ReadAsStringAsync().Result;
                        UserToken token = JsonSerializer.Deserialize<UserToken>(result);

                        if (token.token != "")
                        {
                            apiResult.hasErr = false;
                            apiResult.email = user.email;
                        }
                        else
                        {
                            apiResult.hasErr = true;
                            apiResult.email = "";
                        }

                    }
                    return apiResult;
                }
                catch (Exception ex)
                {
                    apiResult.hasErr = true;
                    apiResult.email = "";

                    return apiResult;
                }
            }
        }
    }
}
